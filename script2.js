'use strict'

var old;

function dijkstra(N, S, matrix, E) {
    fetch('/dijkstra', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            N: N,
            S: S,
            matrix: matrix,
            E: E
        })
    }).then(function(res) {
        return res.json()
    }).then(function(res) {
        
        var css = "";
        if(res.length > E) while (E != S) {
            css += ".l" + res[E] + "-" + E + "," + ".l" + E + "-" + res[E] + ",";

            E = res[E];
        }
        if (old) old();
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = css.slice(0, css.length - 1) + "{stroke: red}";
        style.id = "color";
        document.getElementsByTagName('head')[0].appendChild(style);
        old = () => {
            document.getElementsByTagName('head')[0].removeChild(style);
        }
    }).catch(console.log);

}
var matrix = [],
    number = -1,
    save = [];
var check = localStorage.getItem('save');
if (check) {
    check = JSON.parse(check);
    matrix = JSON.parse(localStorage.getItem('matrix'));
    check.forEach(setNode);


    for (var i = 0; i < matrix.length; i++) {
        for (var j = i; j < matrix.length; j++) {
            (function(i, j) {
                if (matrix[i][j] < 1000000) {
                    var sline = document.createElementNS("http://www.w3.org/2000/svg", "line");
                    sline.setAttribute('class', "l" + i + "-" + j);
                    sline.addEventListener('contextmenu', function(e) {

                        e.preventDefault()
                        var coord = this.getBBox()
                        var value = document.createElement('div');
                        value.className = 'value';
                        value.style.left = coord.x + coord.width / 2 + "px";
                        value.style.top = coord.y + coord.height / 2 + "px";
                        var ves = +prompt('Вес', '');
                        matrix[i][j] = ves;
                        matrix[j][i] = ves;
                        value.innerHTML = ves;
                        document.body.appendChild(value)
                        localStorage.setItem('matrix', JSON.stringify(matrix))
                    })
                    sline.setAttribute('y1', check[i].pageY);
                    sline.setAttribute('x1', check[i].pageX);
                    sline.setAttribute('y2', check[j].pageY);
                    sline.setAttribute('x2', check[j].pageX);
                    sline.innerHTML = matrix[i][j];
                    map.appendChild(sline)
                    if (matrix[i][j] != 1) {
                        var coord = sline.getBBox()
                        var value = document.createElement('div');
                        value.className = 'value';
                        value.style.left = coord.x + coord.width / 2 + "px";
                        value.style.top = coord.y + coord.height / 2 + "px";
                        value.innerHTML = matrix[i][j];
                        document.body.appendChild(value)
                    }
                }
            })(i, j)

        }
    }
    check = false;

}
var line, A = false,
    B = false;
map.addEventListener('click', setNode);


function setNode(e) {
    var node = document.createElement('div');
    number++;
    var _number = number;
    node.id = "node";
    node.innerHTML = _number;
    node.style.top = e.pageY - 15 + 'px';
    node.style.left = e.pageX - 15 + 'px';
    var len = save.length;
    save[len] = {};
    console.log(save)
    save[len].pageX = e.pageX;

    save[len].pageY = e.pageY;
    node.addEventListener('click', function(e) {
        var _B = B;
        if (!line) {
            line = document.createElementNS("http://www.w3.org/2000/svg", "line");
            line.setAttribute('class', "l" + _number + "-");
            line.setAttribute('y1', this.offsetTop + 15);
            line.setAttribute('x1', this.offsetLeft + 15);
            A = _number;
        }
        else if ((B | B === 0) && B != _number) {
            line.addEventListener('contextmenu', function(e) {

                e.preventDefault()
                var coord = this.getBBox()
                var value = document.createElement('div');
                value.className = 'value';
                value.style.left = coord.x + coord.width / 2 + "px";
                value.style.top = coord.y + coord.height / 2 + "px";
                var ves = +prompt('Вес', '');
                matrix[_B][_number] = ves;
                matrix[_number][_B] = ves;
                value.innerHTML = ves;
                document.body.appendChild(value)
                localStorage.setItem('matrix', JSON.stringify(matrix))
            })
            map.appendChild(line);
            line.setAttribute('class', "l" + _number + line.getAttribute('class'));
            line.setAttribute('y1', this.offsetTop + 15);
            line.setAttribute('x1', this.offsetLeft + 15);
            matrix[_B][_number] = 1;
            matrix[_number][_B] = 1;
            localStorage.setItem('matrix', JSON.stringify(matrix))
            line = null;
            A = false;
            B = false;
        }


    });
    node.addEventListener('contextmenu', function(e) {
        var _A = A;
        e.preventDefault()
        if (!line) {
            line = document.createElementNS("http://www.w3.org/2000/svg", "line");
            line.setAttribute('class', "-" + _number);
            line.setAttribute('y2', this.offsetTop + 15);
            line.setAttribute('x2', this.offsetLeft + 15);
            B = _number;
        }
        else if ((A | A === 0) && A != _number) {
            map.appendChild(line);
            line.addEventListener('contextmenu', function(e) {
                e.preventDefault()
                var coord = this.getBBox()
                var value = document.createElement('div');
                value.className = "value";
                value.style.left = coord.x + coord.width / 2 + "px";
                value.style.top = coord.y + coord.height / 2 + "px";
                var ves = +prompt('Вес')
                matrix[_number][_A] = ves;
                matrix[_A][_number] = ves;
                value.innerHTML = ves;
                document.body.appendChild(value);
                localStorage.setItem('matrix', JSON.stringify(matrix))

            })
            line.setAttribute('class', line.getAttribute('class') + _number);
            line.setAttribute('y2', this.offsetTop + 15);
            line.setAttribute('x2', this.offsetLeft + 15);
            matrix[_A][_number] = 1;
            matrix[_number][_A] = 1;
            localStorage.setItem('matrix', JSON.stringify(matrix))
            line = null;
            A = false;
            B = false;
        }

    });
    if (!check) {
        matrix[matrix.length] = Array.apply(null, Array(matrix.length + 1)).map(() => {
            return 1000000
        });
        for (var i = 0; i < matrix.length - 1; i++) {
            matrix[i][matrix[i].length] = 1000000;
        }
    }
    document.body.appendChild(node);
    localStorage.setItem('save', JSON.stringify(save));
    localStorage.setItem('matrix', JSON.stringify(matrix))
    console.log(matrix)

}

function _clear() {
    matrix = [];
    number = -1;
    save = [];
    localStorage.removeItem('matrix');
    localStorage.removeItem('save');
    window.location.reload()
}

send.addEventListener('click', () => {
    dijkstra(number + 1, pointA.value, matrix, pointB.value);
})
clear.addEventListener('click', _clear)