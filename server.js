var http = require('http');
var fs = require('fs');
var port = process.argv[2] || 80;
function dijkstra(N, S, matrix, L) {
    var valid = Array.apply(null, Array(N)).map(() => {
            return true
        }),
        weight = Array.apply(null, Array(N)).map(() => {
            return 1000000
        }),
        prev = [];

    weight[S] = 0;
    for (var k = 0; k < N; k++) {
        var lol = 0
        var minWeight = 1000001,
            ID = -1,
            len = weight.length;
        for (var i = 0; i < len; i++) {
            if (valid[i] && (weight[i] < minWeight)) {
                minWeight = weight[i];
                ID = i;
            }
        }
        for (var i = 0; i < N; i++) {
            // console.log(matrix[ID][i])
            if (weight[ID] + matrix[ID][i] < weight[i]) {
                weight[i] = weight[ID] + matrix[ID][i];
                prev[i] = ID
            }
        }
        valid[ID] = false;
        

    }
    console.log(prev)
        return prev;

}
var server = new http.Server(function(req, res) {
    switch (req.url) {
        case '/':
            fs.readFile('index.html', function(err, buffer) {
                res.setHeader('Cache-Control', 'public, max-age=31557600');
                res.setHeader('Content-Type', 'text/html');
                res.write(buffer);
                res.end();
            });
            break;
        case '/dijkstra':
            var body = [];
            req.on('data', function(chunk) {
                body.push(chunk);
            }).on('end', function() {
                body = Buffer.concat(body).toString();
                var json = JSON.parse(body);
                console.log("json:" + JSON.stringify(json))
                res.end(JSON.stringify(dijkstra(json.N, json.S, json.matrix, json.E)))
            });
            
            break;

        default:
            fs.readFile(req.url.slice(1), function(err, buffer) {
                if (err) {
                    res.statusCode = 404;
                    res.end('something goes wrong')
                }
                try {
                    res.setHeader('Cache-Control', 'public, max-age=31557600');
                }
                catch (err) {
                    console.log(err);
                }
                res.end(buffer);
            });
    }

});
server.listen(port);